% -----------------------------------------------------------------------------
% A procedure to write a video from a sequence of time laps images while
% normalising image intensity using first frame as reference image
% -----------------------------------------------------------------------------
% The directory and subfolders containing the files is initially selected
% via a gui command. In the presented example all files for a sequence have a
% common string that is used to group images belonging together and to
% tansform them to a video. If the image file names are different, the
% workflow needs to be adopted.
% Running the script on a GPU computer may increase speed. 
clear 
% Specify a few variables to be used in the script
fr = 3; % Specify the framerate of the output video, low framerates enable a slow motion effect
% Would be of interest to specify the frame number read from a video into a
% variable here later

% Select directory (including subfolders) with image files from time laps
% recordings
imgdir=uigetdir('Z:\folder\folder','Select parent folder containing videos'); % Opens a GUI to select the directory containing the tif files. 1st entry = default, 2nd entry = message
files = dir(fullfile(imgdir,'**','Img*.tif')); %Writes a file list (structure array) including subfolders of the selected directory but with only files stating with Img
files_table = struct2table(files); % converts the file list to a table

% Sort table according to date/time and exclude certain type of files based on strings
sorttable = sortrows(files_table, "datenum"); % sorts according to data
sorttable2 = sorttable(cellfun(@isempty, strfind(sorttable.name, 'stack')), :);% exclude image stacks in the same directory (files containing the string "stack")
FilePath = string(char(sorttable2.folder + "\" + sorttable2.name));

% Create a unique identifier that marks all files belonging to one time
% laps recording
group = regexprep(FilePath, '--t.+', ''); % converts filename to group identifier
group2 = array2table(group);
FilePath2 = array2table(FilePath);
grouptable = [FilePath2 group2]; % merges the original with the group table
grouplist = unique(group2,'rows');
grouplist2 = transpose(table2cell(grouplist));

savedir=uigetdir(imgdir,'Select folder to save edf images'); % Opens a GUI to select the directory containing the tif files. imgdir represents the default.


% Loop for image reading and video writing, includes mean-normalisation and required conversion to 8bit
f=waitbar(0, 'start', 'Position',[345 41.8750 270 56.2500]);% Creates a progress bar for the loop in the specified position
for i = 1 : length(grouplist2)
     pg=i/length(grouplist2);% Relative loop number
   waitbar(pg,f,'Progress');% Updates the waitbar
groupID = char(grouplist2{i} + ".avi");% the char command is needed to create a file path with single quote. Double quoted file with spaces cannot be written.

% Extract file name from path
FileName = char(regexprep(groupID, '.+\', ''));
% Create name for stabilised video, using the specified directory for the
% filepath
FPName = char(savedir + "\" + FileName);
strtrim(FPName);% removes trailing spaces in the file name
VideoFileName = char(regexprep(FPName, '.avi', ''));
VideoFileName = strtrim(VideoFileName);% removes trailing spaces in the file name

movie_obj = VideoWriter(VideoFileName, 'Uncompressed AVI');
movie_obj.FrameRate = fr; %set the frames per second, specified at beginning of the script
ix = grouptable.group == grouplist2{i}; % Generates the the index rows with certain string in the specified column
subtable = grouptable (ix,:); % creates subtable
image_names = transpose(subtable.FilePath);% create file list as structarray for stacks


first_image_name = image_names{1};% extract name of first frame of image sequence  
first_img = imread(first_image_name); %read first image
imshow(imresize(first_img,0.3)), title(FileName); %displays the merged zstack with reduced size as progress control
refintens = mean2(first_img); % determine intensity to be used as reference for all subsequent images

open(movie_obj);% before writing a video, the video object variable must be opened
for K = 1 : 20(image_names); % K = 1: length (image_names) in case you want to read all frames
  this_img = imread(image_names{K});
  this_img_intens = mean2(this_img); % determine intensity of each image
  img_norm = this_img/(this_img_intens/refintens);% normalise image intensity by reference intensity
  this_image8 = uint8(img_norm/256); % convert to 8-bit, deviding by 256 avoids saturation of images
  writeVideo(movie_obj, this_image8);
end
close(movie_obj);
end
close (f); %closes the waitbar
close all; %closes all images
