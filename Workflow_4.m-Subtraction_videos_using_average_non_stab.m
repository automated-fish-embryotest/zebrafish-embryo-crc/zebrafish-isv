% ----------------------------------------------------------------------
% A script to create subtraction video based on non-stabilised videos
% 
% ----------------------------------------------------------------------
% 
% The directory containing the video files is initially selected
% via a gui command. 
clear
% Specify a few variables to be used in the script
fr = 1; % Specify the framerate of the output video, low framerates enable a slow motion effect
sigma = 1; % Gaussian blur factor, may require optimisation
thresholdValue = 10; % used for creation of binary images, may need optimisation

addpath('Z:\MatLab scripts'); % Indicate the path to the required functions cvexEstStabilizationTform.m, cvexTformToSRT.m
imgdir=uigetdir('Z:\folder\folder', 'Select parent folder containing the videos'); % Opens a GUI to select the directory containing the .avi video files
% Change the _stab.avi to avi. if you want to analyse the non-stabilised
% images
files = dir(fullfile(imgdir,'**','*.avi')); %Writes a file list (structure array) including subfolders of the selected directory but with only file estentions "stab.avi".
table = struct2table(files); % converts the file list to a table
subtable = table(cellfun(@isempty, strfind(table.name, 'sub')), :);% exclude videos created previously from image stacks in the same directory (files containing the string "stack")
% Optional: if you want to analyse non-stabilised videos
subtable = subtable(cellfun(@isempty, strfind(subtable.name, 'stab')), :);% exclude stabilised videos (files containing the string "stab")
FilePath = string(char(subtable.folder + "\" + subtable.name));
FilePath = array2table(FilePath);
FilePath.Properties.VariableNames = {'name'};

%subtable2=string(subtable.name); % extracts filenames into a string
FilePath=table2cell(FilePath);
FilePath=transpose(FilePath);

savedir=uigetdir(imgdir, 'Select folder for savin subtraction videos and images'); % Opens a GUI to select the directory containing the .avi video files


mkdir Z:\VAST\Julia\Angiogenesis_Inhibitors\27112020_SU4312_0-96hpf\SU4312_0-96hpf_Auswertung; % in case the directory does not already exist, use a
% user specific directory if working on a server or computer is used by
% different users
cd Z:\VAST\Julia\Angiogenesis_Inhibitors\27112020_SU4312_0-96hpf\SU4312_0-96hpf_Auswertung\Temp; % Use as temporary working directory

% Read all frames of a video
% Loop for image reading and video writing
f=waitbar(0, 'start', 'Position',[345 41.8750 270 56.2500]);% Creates a progress bar for the loop in the specified position

for i = 1 : length(FilePath)
delete *.*; % delete any remaining files from a previous analysis
  pg=i/length(FilePath);% Relative loop number
   waitbar(pg,f,'Progress');% Updates the waitbar
% Read the video into a video object (here called vo)
vo=VideoReader(FilePath{i});

%Calculate average image. 

%To avoid image saturation, each frame is divided
%by the total number of frames. Therefore, summing up these images results
%in the average of images. If the images are added without dividing, a
%saturation effect will change the images.

for nof = 1:vo.NumberOfFrames
       thisImage = read (vo, nof);
       thisImage = imgaussfilt(thisImage,sigma); % apply Gaussian  Blurr       
       thisImage = thisImage/vo.NumberOfFrames;
       if nof == 1
    avImage = thisImage;
  else
    avImage = avImage + thisImage;
  end
end

%Subtract each image of the frame from the average
for nof = 1:vo.NumberOfFrames
       thisImage = read (vo, nof);
       blurImage = imgaussfilt(thisImage,sigma); % apply a gaussian blur to the image
%bluravImage = imgaussfilt(avImage,2); % apply a gaussian blur to the image
subImage = blurImage - avImage; %subtract and multiply image
greyImage = rgb2gray (subImage);

binaryImage = greyImage > thresholdValue; % Bright objects will be chosen if you use >.
filename = strcat('frame', num2str(nof),'.tif');
imwrite (binaryImage, filename);     
end

% Sort frames by extracting frame number and provide a cellarray of frames
% for subsequent video writing
files = dir ('*.tif'); %Writes a file list (structure array) including subfolders of the selected directory but with only file estentions ".avi".
files2 = struct2table(files); % converts the file list to a table
files3 = files2.name;
files3 = regexprep(files3, 'frame', ''); % converts filename to group identifier
files3 = regexprep(files3, '.tif', ''); % converts filename to group identifier
files3=str2double(files3);
files3 = array2table(files3);
files4 = [files2 files3];
files5 = sortrows(files4, "files3");
files6 = transpose(table2cell(files5(:,1)));

% Write a video from subtraction threshold images
%groupID = char(grouplist2{i} + ".avi");% the char command is needed to create a file path with single quote. Double quoted file with spaces cannot be written.
VideoName = FilePath{i};
newVideoName = char(regexprep(VideoName, '.+\', ''));
newVideoName = char(savedir + "\" + newVideoName);% Merges selected directory savedir and filename to a full file path
newVideoName = strtrim(newVideoName);% removes trailing spaces in the file name
newVideoName = regexprep(newVideoName, '.avi', '_sub'); %file extension is automatically added
movie_obj = VideoWriter(newVideoName, 'Uncompressed AVI');
movie_obj.FrameRate = fr; %set the frames per second
open(movie_obj);% before writing a video, the video object variable must be opened
for K = 1 : length(files6) % K = 1: length (image_names) in case you want to read all frames
  this_image = imread(files6{K});
  this_image8 = (uint8(this_image))*1000; % convert to 8-bit, deviding by 256 avoids saturation of images
  writeVideo(movie_obj, this_image8);
end
close(movie_obj);


% Write the sum/maximum of all threshold images
for K = 1 : length(files6) % K = 1: length (image_names) in case you want to read all frames
       thisImage2 = imread(files6{K});
       %thisImage = imgaussfilt(thisImage,sigma); % apply Gaussian  Blurr       
       %thisImage = thisImage/vo.NumberOfFrames;
       if K == 1
    avImage2 = thisImage2;
  else
    avImage2 = avImage2 + thisImage2;
  end
end
VideoName = FilePath{i};
subImageName = regexprep(newVideoName, 'sub', '_sub.tif'); % converts filename to group identifier, filename needs to have extension
imshow(imresize(avImage2,0.3)), title (subImageName); % displays max of subtraction image to show progress
imwrite(avImage2,subImageName,'tif'); %Write to the specified filename in the original path
end
close (f); %closes the waitbar
close all; % closes all images
