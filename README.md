# Assessment of chemical effects on intersegmental vessel development in zebrafish embryos

Using a video subtraction process (details described elsewhere) the effects of chemicals on the development of the cardiovascular system in zebrafish can be detect. The quantitative assessment of subtraction images requires a set of different workflows established in KNIME (version 4.5) and Matlab (version 2019a). Other versions of KNIME and Matlab may be used as well but have not been tested. The KNIME workflows use embedded R-scripts. Hence, in order to execute the KNIME script an R installation (3.6 or later) and appropriate packages (drc) are required.

## KNIME workflows for the assessment of ISVs

- Workflow 1_feature extraction.knwf (KNIME)
  This workflow extracts the feature and metrics for various structural features in zebrafish embryos analysed with the software FishInspector 1.70
- Workflow 2- heart rate.knwf (KNIME)
  Worklow to extract the heart rate from zebrafish embryo videos using a fourier transformation.
- Workflow 3.m-Create_uncompressed_intesity_comp_video_from_tiff_fullpath.m (Matlab)
  Workflow to create a video (avi format) from timelaps recordings (tiff) of a microscope
- Workflow 4.m-Subtraction_videos_using_average_non_stab.m (Matlab)
  Worklfow to create a subtraction image from an avi video in order to display the cardiovascular system.
- Workflow 5- number_ISV.knwf (KNIME)
  Worklow to extract the number ISVs from the manual annotation of ISVs in the software FishInspector 1.70
- Workflow 6-ISV_DRC.knwf (KNIME)
  Workflow to derive concentration-response models and calculate effect concentrations for the number of ISVs
- Workflow 7-Control-Variability.knwf (KNIME)
  Workflow to derive control variabilities of morphological features. The workflow is required for concentration-response modelling
  of annotated morphological features and the metrics obtained by the software FishInspector.




